<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<body>
<head>

    <link href='<spring:url value="/resources/css/my-test.css"/>' rel="stylesheet" />
    <script type="text/javascript" src='<spring:url value="/resources/js/simple-test.js"/>'></script>


</head>


<input type="button" onclick="doSomeWork()" value="Click Me"/>

<h2>Spring MVC Demo - Home Page</h2>

<a href="/showForm">Hello World form</a>


<br>
<img alt="renskies.com" src="<spring:url value="/images/img01.jpg"/>" width="200">

</body>
</html>
